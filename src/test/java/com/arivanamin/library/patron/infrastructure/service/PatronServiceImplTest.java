package com.arivanamin.library.patron.infrastructure.service;

import com.arivanamin.library.patron.infrastructure.repository.PatronRepository;
import com.arivanamin.library.patron.model.entity.Patron;
import com.arivanamin.library.patron.model.service.PatronNotFoundException;
import com.github.javafaker.Faker;
import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentCaptor.forClass;
import static org.mockito.Mockito.*;

@ExtendWith (MockitoExtension.class)
class PatronServiceImplTest {
    
    private final Faker faker = new Faker();
    private final EasyRandom random = new EasyRandom();
    
    @Mock
    private PatronRepository repository;
    
    @InjectMocks
    private PatronServiceImpl service;
    
    private List<Patron> patrons;
    private ArgumentCaptor<List> listCaptor;
    
    @BeforeEach
    void setUp () {
        patrons = new ArrayList<>();
        listCaptor = forClass(List.class);
        
        int numberOfPatrons = faker.number().numberBetween(3, 5);
        for (int i = 0; i < numberOfPatrons; i++) {
            Patron patron = random.nextObject(Patron.class);
            patrons.add(patron);
        }
    }
    
    @AfterEach
    void tearDown () {
    }
    
    @Test
    @DisplayName ("test that getAllPatrons returns the list from repository")
    void testThatGetAllPatronsFetchesPatronsFromRepository () {
        when(repository.findAll()).thenReturn(patrons);
        List<Patron> allPatrons = service.getAllPatrons();
        verify(repository, times(1)).findAll();
        assertSame(patrons, allPatrons);
    }
    
    @Test
    @DisplayName ("test that getPatronDetails returns result of repository findById method")
    void testThatGetPatronDetailsFetchesSinglePatronFromRepository () {
        long patronId = 7;
        when(repository.findPatronById(patronId)).thenReturn(Optional.of(patrons.get(0)));
        Patron patron = service.getPatronDetails(patronId);
        verify(repository, times(1)).findPatronById(patronId);
        assertSame(patrons.get(0), patron);
    }
    
    @Test
    @DisplayName ("test that getPatronDetails throws exception when patron is not found")
    void testThatGetPatronDetailsThrowsExceptionWhenNotFound () {
        assertThrows(PatronNotFoundException.class, () -> {
            service.getPatronDetails(7);
        });
    }
    
    @Test
    @DisplayName ("test that createPatron send patron to repository to be saved")
    void testThatCreatePatronSavesEntityInRepository () {
        service.createPatron(patrons.get(0));
        verify(repository, times(1)).save(patrons.get(0));
    }
    
    @Test
    @DisplayName ("test that deletePatron send patron id to repository to be deleted")
    void testThatDeletePatronRemovedEntityInRepository () {
        long patronId = faker.number().numberBetween(10, 100);
        service.deletePatron(patronId);
        verify(repository, times(1)).deleteById(patronId);
    }
    
    @Test
    @DisplayName ("test that updatePatron updates patron in repository when patron id exists")
    void testThatUpdatePatronFetchesSinglePatronFromRepository () {
        long patronId = 7;
        when(repository.findPatronById(patronId)).thenReturn(Optional.of(patrons.get(0)));
        service.updatePatron(patronId, patrons.get(0));
        verify(repository, times(1)).findPatronById(patronId);
        verify(repository, times(1)).save(patrons.get(0));
    }
    
    @Test
    @DisplayName ("test that updatePatron throws exception when patron is not found")
    void testThatUpdatePatronThrowsExceptionWhenNotFound () {
        assertThrows(PatronNotFoundException.class, () -> {
            service.getPatronDetails(7);
        });
    }
}
