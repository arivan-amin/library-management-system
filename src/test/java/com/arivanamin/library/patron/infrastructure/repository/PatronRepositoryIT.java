package com.arivanamin.library.patron.infrastructure.repository;

import com.arivanamin.library.patron.model.entity.Patron;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class PatronRepositoryIT {
    
    @Autowired
    private PatronRepository repository;
    
    private Patron patron;
    private Faker faker;
    
    @BeforeEach
    void setUp () {
        faker = new Faker();
        patron = new Patron();
        patron.setName(faker.elderScrolls().race());
        patron.setContactInformation(faker.company().catchPhrase());
    }
    
    @AfterEach
    void tearDown () {
        repository.deleteAll();
    }
    
    @Test
    @DisplayName ("findPatronById returns the patron when id exists in the repository")
    void testThatFindPatronByIdWhenPatronExists () {
        repository.save(patron);
        Long patronId = repository.findAll().get(0).getId();
        Optional<Patron> patronOptional = repository.findPatronById(patronId);
        assertTrue(patronOptional.isPresent());
    }
    
    @Test
    @DisplayName ("findPatronById returns empty optional when id doesn't exist")
    void testFindPatronByIdWhenPatronExists () {
        Optional<Patron> patronOptional = repository.findPatronById(1L);
        assertFalse(patronOptional.isPresent());
    }
}
