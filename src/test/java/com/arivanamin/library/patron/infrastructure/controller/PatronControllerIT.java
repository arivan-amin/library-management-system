package com.arivanamin.library.patron.infrastructure.controller;

import com.arivanamin.library.patron.infrastructure.repository.PatronRepository;
import com.arivanamin.library.patron.model.entity.Patron;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class PatronControllerIT {
    
    private final Faker faker = new Faker();
    
    @Autowired
    private MockMvc mockMvc;
    
    @Autowired
    private ObjectMapper mapper;
    
    @Autowired
    private PatronRepository repository;
    
    @BeforeEach
    void setUp () {
        // delete the elements inserted by the command line runner that initializes fake elements
        // for demo
        repository.deleteAll();
    }
    
    @AfterEach
    void tearDown () {
        repository.deleteAll();
    }
    
    @Test
    @DisplayName ("test that getAllPatrons api returns all patrons")
    void testGetAllPatronsEndpoint () throws Exception {
        int numberOfEntities = faker.number().numberBetween(3, 7);
        insertTestPatronsIntoRepository(numberOfEntities);
        MvcResult mvcResult = mockMvc.perform(get("/api/patrons").contentType(APPLICATION_JSON))
            .andExpect(status().isOk()).andReturn();
        String content = mvcResult.getResponse().getContentAsString();
        List<Patron> patrons = mapper.readValue(content, new TypeReference<>() {
        });
        assertEquals(numberOfEntities, patrons.size());
    }
    
    private void insertTestPatronsIntoRepository (int numberOfEntities) {
        for (int i = 0; i < numberOfEntities; i++) {
            Patron patron = createPatron();
            repository.save(patron);
        }
    }
    
    private Patron createPatron () {
        return Patron.builder().name(faker.zelda().character())
            .contactInformation(faker.commerce().productName()).build();
    }
    
    @Test
    @DisplayName ("test that getPatronDetails api returns single patron details by id")
    void testGetSinglePatronsDetailsEndpoint () throws Exception {
        insertTestPatronsIntoRepository(1);
        Long patronId = repository.findAll().get(0).getId();
        MvcResult mvcResult =
            mockMvc.perform(get("/api/patrons/" + patronId).contentType(APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        String content = mvcResult.getResponse().getContentAsString();
        Patron patron = mapper.readValue(content, new TypeReference<>() {
        });
        assertEquals(patronId, patron.getId());
    }
    
    @Test
    @DisplayName ("test that getPatronDetails api returns 404 when patron not found")
    void testGetSinglePatronsDetailsEndpointWhenPatronNotFound () throws Exception {
        mockMvc.perform(get("/api/patrons/1").contentType(APPLICATION_JSON))
            .andExpect(status().isNotFound());
    }
    
    @Test
    @DisplayName ("test that createPatron api creates a patron in repository")
    void testCreatePatronEndpointWithPost () throws Exception {
        Patron patron = createPatron();
        String patronString = mapper.writeValueAsString(patron);
        mockMvc.perform(post("/api/patrons").contentType(APPLICATION_JSON).content(patronString))
            .andExpect(status().isCreated());
        assertEquals(1, repository.findAll().size());
    }
    
    @Test
    @DisplayName ("test that updatePatron api updates a patron in repository")
    void testUpdatePatronEndpointWithPut () throws Exception {
        insertTestPatronsIntoRepository(1);
        Long patronId = repository.findAll().get(0).getId();
        Patron patron = createPatron();
        String updatedName = "updated name";
        patron.setName(updatedName);
        String patronString = mapper.writeValueAsString(patron);
        mockMvc.perform(
                put("/api/patrons/" + patronId).contentType(APPLICATION_JSON).content(patronString))
            .andExpect(status().isCreated());
        Patron updatedPatron = repository.findAll().get(0);
        assertEquals(1, repository.findAll().size());
        assertEquals(updatedName, repository.findAll().get(0).getName());
    }
    
    @Test
    @DisplayName ("test that updatePatron api returns 404 when patron not found")
    void testUpdatePatronEndpointWhenPatronNotFound () throws Exception {
        Patron patron = createPatron();
        String patronString = mapper.writeValueAsString(patron);
        mockMvc.perform(put("/api/patrons/1").contentType(APPLICATION_JSON).content(patronString))
            .andExpect(status().isNotFound());
    }
    
    @Test
    @DisplayName ("test that deletePatron api deletes a single patron in repository")
    void testDeletePatronEndpoint () throws Exception {
        insertTestPatronsIntoRepository(1);
        Long patronId = repository.findAll().get(0).getId();
        MvcResult mvcResult =
            mockMvc.perform(delete("/api/patrons/" + patronId).contentType(APPLICATION_JSON))
                .andExpect(status().isNoContent()).andReturn();
        assertTrue(repository.findAll().isEmpty());
    }
}
