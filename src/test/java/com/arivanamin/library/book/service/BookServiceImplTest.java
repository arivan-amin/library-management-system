package com.arivanamin.library.book.service;

import com.arivanamin.library.book.infrastructure.repository.BookRepository;
import com.arivanamin.library.book.infrastructure.service.BookServiceImpl;
import com.arivanamin.library.book.model.entity.Book;
import com.arivanamin.library.book.model.service.BookNotFoundException;
import com.github.javafaker.Faker;
import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentCaptor.forClass;
import static org.mockito.Mockito.*;

@ExtendWith (MockitoExtension.class)
class BookServiceImplTest {
    
    private final Faker faker = new Faker();
    private final EasyRandom random = new EasyRandom();
    
    @Mock
    private BookRepository repository;
    
    @InjectMocks
    private BookServiceImpl service;
    
    private List<Book> books;
    private ArgumentCaptor<List> listCaptor;
    
    @BeforeEach
    void setUp () {
        books = new ArrayList<>();
        listCaptor = forClass(List.class);
        
        int numberOfBooks = faker.number().numberBetween(3, 5);
        for (int i = 0; i < numberOfBooks; i++) {
            Book book = random.nextObject(Book.class);
            books.add(book);
        }
    }
    
    @AfterEach
    void tearDown () {
    }
    
    @Test
    @DisplayName ("test that getAllBooks returns the list from repository")
    void testThatGetAllBooksFetchesBooksFromRepository () {
        when(repository.findAll()).thenReturn(books);
        List<Book> allBooks = service.getAllBooks();
        verify(repository, times(1)).findAll();
        assertSame(books, allBooks);
    }
    
    @Test
    @DisplayName ("test that getBookDetails returns result of repository findById method")
    void testThatGetBookDetailsFetchesSingleBookFromRepository () {
        long bookId = 7;
        when(repository.findBookById(bookId)).thenReturn(Optional.of(books.get(0)));
        Book book = service.getBookDetails(bookId);
        verify(repository, times(1)).findBookById(bookId);
        assertSame(books.get(0), book);
    }
    
    @Test
    @DisplayName ("test that getBookDetails throws exception when book is not found")
    void testThatGetBookDetailsThrowsExceptionWhenNotFound () {
        assertThrows(BookNotFoundException.class, () -> {
            service.getBookDetails(7);
        });
    }
    
    @Test
    @DisplayName ("test that createBook send book to repository to be saved")
    void testThatCreateBookSavesEntityInRepository () {
        service.createBook(books.get(0));
        verify(repository, times(1)).save(books.get(0));
    }
    
    @Test
    @DisplayName ("test that deleteBook send book id to repository to be deleted")
    void testThatDeleteBookRemovedEntityInRepository () {
        long bookId = faker.number().numberBetween(10, 100);
        service.deleteBook(bookId);
        verify(repository, times(1)).deleteById(bookId);
    }
    
    @Test
    @DisplayName ("test that updateBook updates book in repository when book id exists")
    void testThatUpdateBookFetchesSingleBookFromRepository () {
        long bookId = 7;
        when(repository.findBookById(bookId)).thenReturn(Optional.of(books.get(0)));
        service.updateBook(bookId, books.get(0));
        verify(repository, times(1)).findBookById(bookId);
        verify(repository, times(1)).save(books.get(0));
    }
    
    @Test
    @DisplayName ("test that updateBook throws exception when book is not found")
    void testThatUpdateBookThrowsExceptionWhenNotFound () {
        assertThrows(BookNotFoundException.class, () -> {
            service.getBookDetails(7);
        });
    }
}
