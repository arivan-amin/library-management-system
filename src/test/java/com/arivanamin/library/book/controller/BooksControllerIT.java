package com.arivanamin.library.book.controller;

import com.arivanamin.library.book.model.entity.Book;
import com.arivanamin.library.book.infrastructure.repository.BookRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class BooksControllerIT {
    
    private final Faker faker = new Faker();
    
    @Autowired
    private MockMvc mockMvc;
    
    @Autowired
    private ObjectMapper mapper;
    
    @Autowired
    private BookRepository repository;
    
    @BeforeEach
    void setUp () {
        // delete the elements inserted by the command line runner that initializes fake elements
        // for demo
        repository.deleteAll();
    }
    
    @AfterEach
    void tearDown () {
        repository.deleteAll();
    }
    
    @Test
    @DisplayName ("test that getAllBooks api returns all books")
    void testGetAllBooksEndpoint () throws Exception {
        int numberOfEntities = faker.number().numberBetween(3, 7);
        insertTestBooksIntoRepository(numberOfEntities);
        MvcResult mvcResult = mockMvc.perform(get("/api/books").contentType(APPLICATION_JSON))
            .andExpect(status().isOk()).andReturn();
        String content = mvcResult.getResponse().getContentAsString();
        List<Book> books = mapper.readValue(content, new TypeReference<>() {
        });
        assertEquals(numberOfEntities, books.size());
    }
    
    private void insertTestBooksIntoRepository (int numberOfEntities) {
        for (int i = 0; i < numberOfEntities; i++) {
            Book book = createBook();
            repository.save(book);
        }
    }
    
    private Book createBook () {
        return Book.builder().isbn(faker.numerify("#############"))
            .title(faker.elderScrolls().dragon()).author(faker.book().author())
            .publicationYear("2020").build();
    }
    
    @Test
    @DisplayName ("test that getBookDetails api returns single book details by id")
    void testGetSingleBooksDetailsEndpoint () throws Exception {
        insertTestBooksIntoRepository(1);
        Long bookId = repository.findAll().get(0).getId();
        MvcResult mvcResult =
            mockMvc.perform(get("/api/books/" + bookId).contentType(APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        String content = mvcResult.getResponse().getContentAsString();
        Book book = mapper.readValue(content, new TypeReference<>() {
        });
        assertEquals(bookId, book.getId());
    }
    
    @Test
    @DisplayName ("test that getBookDetails api returns 404 when book not found")
    void testGetSingleBooksDetailsEndpointWhenBookNotFound () throws Exception {
        mockMvc.perform(get("/api/books/1").contentType(APPLICATION_JSON))
            .andExpect(status().isNotFound());
    }
    
    @Test
    @DisplayName ("test that createBook api creates a book in repository")
    void testCreateBookEndpointWithPost () throws Exception {
        Book book = createBook();
        String bookString = mapper.writeValueAsString(book);
        mockMvc.perform(post("/api/books").contentType(APPLICATION_JSON).content(bookString))
            .andExpect(status().isCreated());
        assertEquals(1, repository.findAll().size());
    }
    
    @Test
    @DisplayName ("test that updateBook api updates a book in repository")
    void testUpdateBookEndpointWithPut () throws Exception {
        insertTestBooksIntoRepository(1);
        Long bookId = repository.findAll().get(0).getId();
        Book book = createBook();
        String updatedTitle = "updated title";
        book.setTitle(updatedTitle);
        String bookString = mapper.writeValueAsString(book);
        mockMvc.perform(
                put("/api/books/" + bookId).contentType(APPLICATION_JSON).content(bookString))
            .andExpect(status().isCreated());
        Book updatedBook = repository.findAll().get(0);
        assertEquals(1, repository.findAll().size());
        assertEquals(updatedTitle, repository.findAll().get(0).getTitle());
    }
    
    @Test
    @DisplayName ("test that updateBook api returns 404 when book not found")
    void testUpdateBookEndpointWhenBookNotFound () throws Exception {
        Book book = createBook();
        String bookString = mapper.writeValueAsString(book);
        mockMvc.perform(put("/api/books/1").contentType(APPLICATION_JSON).content(bookString))
            .andExpect(status().isNotFound());
    }
    
    @Test
    @DisplayName ("test that deleteBook api deletes a single book in repository")
    void testDeleteBookEndpoint () throws Exception {
        insertTestBooksIntoRepository(1);
        Long bookId = repository.findAll().get(0).getId();
        MvcResult mvcResult =
            mockMvc.perform(delete("/api/books/" + bookId).contentType(APPLICATION_JSON))
                .andExpect(status().isNoContent()).andReturn();
        assertTrue(repository.findAll().isEmpty());
    }
}
