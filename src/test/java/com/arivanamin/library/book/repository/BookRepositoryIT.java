package com.arivanamin.library.book.repository;

import com.arivanamin.library.book.infrastructure.repository.BookRepository;
import com.arivanamin.library.book.model.entity.Book;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class BookRepositoryIT {
    
    @Autowired
    private BookRepository repository;
    
    private Book book;
    private Faker faker;
    
    @BeforeEach
    void setUp () {
        faker = new Faker();
        book = new Book();
        book.setIsbn(faker.numerify("#############"));
        book.setTitle(faker.book().title());
        book.setAuthor(faker.book().author());
        book.setPublicationYear(faker.numerify("####"));
    }
    
    @AfterEach
    void tearDown () {
        repository.deleteAll();
    }
    
    @Test
    @DisplayName ("findBookById returns the book when id exists in the repository")
    void testThatFindBookByIdWhenBookExists () {
        repository.save(book);
        Long bookId = repository.findAll().get(0).getId();
        Optional<Book> bookOptional = repository.findBookById(bookId);
        assertTrue(bookOptional.isPresent());
    }
    
    @Test
    @DisplayName ("findBookById returns empty optional when id doesn't exist")
    void testFindBookByIdWhenBookExists () {
        Optional<Book> bookOptional = repository.findBookById(1L);
        assertFalse(bookOptional.isPresent());
    }
}
