package com.arivanamin.library.borrowing.infrastructure.service;

import com.arivanamin.library.book.model.entity.Book;
import com.arivanamin.library.book.model.service.BookService;
import com.arivanamin.library.borrowing.infrastructure.repository.BorrowingRecordRepository;
import com.arivanamin.library.borrowing.model.entity.BorrowingRecord;
import com.arivanamin.library.borrowing.model.service.*;
import com.arivanamin.library.patron.model.entity.Patron;
import com.arivanamin.library.patron.model.service.PatronService;
import com.github.javafaker.Faker;
import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith (MockitoExtension.class)
class BorrowingRecordServiceImplTest {
    
    private final Faker faker = new Faker();
    private final EasyRandom random = new EasyRandom();
    
    @Mock
    private BookService bookService;
    
    @Mock
    private PatronService patronService;
    
    @Mock
    private BorrowingRecordRepository repository;
    
    @InjectMocks
    private BorrowingRecordServiceImpl service;
    
    @BeforeEach
    void setUp () {
    }
    
    @Test
    @DisplayName ("test that borrowBook gets Book details from book Service")
    void testThatBorrowBookGetsBookDetails () {
        mockBookAndPatronServicesToReturnValidEntities();
        long bookId = 175;
        service.borrowBook(bookId, 2);
        verify(bookService, times(1)).getBookDetails(bookId);
    }
    
    private void mockBookAndPatronServicesToReturnValidEntities () {
        when(bookService.getBookDetails(anyLong())).thenReturn(new Book());
        when(patronService.getPatronDetails(anyLong())).thenReturn(new Patron());
    }
    
    @Test
    @DisplayName ("test that borrowBook gets Patron details from Patron Service")
    void testThatBorrowBookGetsPatronDetails () {
        mockBookAndPatronServicesToReturnValidEntities();
        long patronId = 175;
        service.borrowBook(1, patronId);
        verify(patronService, times(1)).getPatronDetails(patronId);
    }
    
    @Test
    @DisplayName ("test that borrowBook throws exception if is book already borrowed")
    void testThatBorrowBookThrowsExceptionForBorrowedBook () {
        mockBookAndPatronServicesToReturnValidEntities();
        when(repository.findAllByBookIdAndPatronIdAndReturningDateNull(anyLong(),
            anyLong())).thenReturn(Optional.of(new BorrowingRecord()));
        assertThrows(BookAlreadyBorrowedException.class, () -> service.borrowBook(1, 2));
    }
    
    @Test
    @DisplayName ("test that borrowBook saves borrowing record when request is valid")
    void testThatBorrowBookSavesValidRequests () {
        mockBookAndPatronServicesToReturnValidEntities();
        service.borrowBook(1, 2);
        verify(repository, times(1)).save(any());
    }
    
    @Test
    @DisplayName ("test that returnBook throws exception if is book already returned")
    void testThatReturnBookThrowsExceptionForReturnedBooks () {
        when(repository.findAllByBookIdAndPatronId(anyLong(), anyLong())).thenReturn(
            List.of(new BorrowingRecord()));
        when(repository.findAllByBookIdAndPatronIdAndReturningDateNull(anyLong(),
            anyLong())).thenReturn(Optional.empty());
        assertThrows(BookAlreadyReturnedException.class, () -> service.returnBook(1, 2));
    }
    
    @Test
    @DisplayName ("test that returnBook throws exception if record not found for book and patron")
    void testThatReturnBookThrowsExceptionForRecordNotFound () {
        when(repository.findAllByBookIdAndPatronId(anyLong(), anyLong())).thenReturn(
            Collections.emptyList());
        assertThrows(NoBorrowingRecordFoundException.class, () -> service.returnBook(1, 2));
    }
    
    @Test
    @DisplayName ("test that returnBook updates status of borrowing record")
    void testThatReturnBookUpdatesStatusOfBorrowingRecord () {
        BorrowingRecord record = new BorrowingRecord();
        when(repository.findAllByBookIdAndPatronId(anyLong(), anyLong())).thenReturn(
            List.of(new BorrowingRecord()));
        
        when(repository.findAllByBookIdAndPatronIdAndReturningDateNull(anyLong(),
            anyLong())).thenReturn(Optional.of(record));
        service.returnBook(1, 2);
        
        ArgumentCaptor<BorrowingRecord> captor = ArgumentCaptor.forClass(BorrowingRecord.class);
        verify(repository, times(1)).save(captor.capture());
        assertNotNull(captor.getValue().getReturningDate());
    }
}
