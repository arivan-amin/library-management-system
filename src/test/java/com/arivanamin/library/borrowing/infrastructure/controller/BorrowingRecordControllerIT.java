package com.arivanamin.library.borrowing.infrastructure.controller;

import com.arivanamin.library.book.infrastructure.repository.BookRepository;
import com.arivanamin.library.book.model.entity.Book;
import com.arivanamin.library.borrowing.infrastructure.repository.BorrowingRecordRepository;
import com.arivanamin.library.borrowing.model.entity.BorrowingRecord;
import com.arivanamin.library.patron.infrastructure.repository.PatronRepository;
import com.arivanamin.library.patron.model.entity.Patron;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class BorrowingRecordControllerIT {
    
    private final Faker faker = new Faker();
    
    @Autowired
    private MockMvc mockMvc;
    
    @Autowired
    private ObjectMapper mapper;
    
    @Autowired
    private BorrowingRecordRepository borrowingRepository;
    
    @Autowired
    private PatronRepository patronRepository;
    
    @Autowired
    private BookRepository bookRepository;
    private Long bookId;
    private Long patronId;
    
    @BeforeEach
    void setUp () {
        borrowingRepository.deleteAll();
        patronRepository.deleteAll();
        bookRepository.deleteAll();
        insertTestBookIntoRepository();
        insertTestPatronIntoRepository();
        bookId = bookRepository.findAll().get(0).getId();
        patronId = patronRepository.findAll().get(0).getId();
    }
    
    private void insertTestBookIntoRepository () {
        Book book = createBook();
        bookRepository.save(book);
    }
    
    private void insertTestPatronIntoRepository () {
        Patron patron = createPatron();
        patronRepository.save(patron);
    }
    
    private Book createBook () {
        return Book.builder().isbn(faker.numerify("#############"))
            .title(faker.elderScrolls().dragon()).author(faker.book().author())
            .publicationYear("2020").build();
    }
    
    private Patron createPatron () {
        return Patron.builder().name(faker.zelda().character())
            .contactInformation(faker.commerce().productName()).build();
    }
    
    @AfterEach
    void tearDown () {
        borrowingRepository.deleteAll();
        patronRepository.deleteAll();
        bookRepository.deleteAll();
    }
    
    @Test
    @DisplayName ("test that borrowBook api creates borrowing record for valid request")
    void testBorrowBookEndpointForValidRequest () throws Exception {
        mockMvc.perform(post(createBorrowUri(bookId, patronId)).contentType(APPLICATION_JSON))
            .andExpect(status().isCreated());
    }
    
    private static String createBorrowUri (long bookId, long patronId) {
        return "/api/borrow/" + bookId + "/patron/" + patronId;
    }
    
    @Test
    @DisplayName ("test that borrowBook returns error when book already borrowed")
    void testBorrowBookEndpointForAlreadyBorrowedBook () throws Exception {
        createBorrowRecordAndSaveToRepository();
        mockMvc.perform(post(createBorrowUri(bookId, patronId)).contentType(APPLICATION_JSON))
            .andExpect(status().isConflict());
    }
    
    private void createBorrowRecordAndSaveToRepository () {
        BorrowingRecord record = new BorrowingRecord();
        record.setBorrowingDate(LocalDate.now());
        record.setBook(bookRepository.findAll().get(0));
        record.setPatron(patronRepository.findAll().get(0));
        borrowingRepository.save(record);
    }
    
    @Test
    @DisplayName ("test that returnBook api sets record status to returned for valid request")
    void testReturnBookEndpointForValidRequest () throws Exception {
        createBorrowRecordAndSaveToRepository();
        mockMvc.perform(put(createReturnUri(bookId, patronId)).contentType(APPLICATION_JSON))
            .andExpect(status().isOk());
    }
    
    private static String createReturnUri (long bookId, long patronId) {
        return "/api/return/" + bookId + "/patron/" + patronId;
    }
    
    @Test
    @DisplayName ("test that returnBook api returns error when book already returned")
    void testReturnBookEndpointForBookAlreadyReturned () throws Exception {
        createReturnedBookRecord();
        mockMvc.perform(put(createReturnUri(bookId, patronId)).contentType(APPLICATION_JSON))
            .andExpect(status().isConflict());
    }
    
    private void createReturnedBookRecord () {
        BorrowingRecord record = new BorrowingRecord();
        record.setBook(bookRepository.findAll().get(0));
        record.setPatron(patronRepository.findAll().get(0));
        record.setBorrowingDate(LocalDate.now());
        record.setReturningDate(LocalDate.now());
        borrowingRepository.save(record);
    }
}
