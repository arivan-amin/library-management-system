package com.arivanamin.library.book.model.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import lombok.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class Book {
    
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotBlank
    @Pattern (regexp = "^(?=(?:\\D*\\d){10}(?:(?:\\D*\\d){3})?$)[\\d-]+$",
        message = "Must be a valid ISBN")
    @Column
    private String isbn;
    
    @NotBlank
    @Column
    private String title;
    
    @NotBlank
    @Column
    private String author;
    
    @NotBlank
    @Column
    @Digits (integer = 4, fraction = 0, message = "Provide a valid year")
    private String publicationYear;
}
