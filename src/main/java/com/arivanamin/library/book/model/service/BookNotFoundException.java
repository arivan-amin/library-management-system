package com.arivanamin.library.book.model.service;

public class BookNotFoundException extends IllegalArgumentException {
    
    public BookNotFoundException () {
        super("Requested Book couldn't be found");
    }
}
