package com.arivanamin.library.book.model.service;

import com.arivanamin.library.book.model.entity.Book;

import java.util.List;

public interface BookService {
    
    List<Book> getAllBooks ();
    
    Book getBookDetails (long id);
    
    void createBook (Book book);
    
    void updateBook (long id, Book updatedBook);
    
    void deleteBook (long id);
}
