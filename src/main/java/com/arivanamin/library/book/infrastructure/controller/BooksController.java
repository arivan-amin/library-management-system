package com.arivanamin.library.book.infrastructure.controller;

import com.arivanamin.library.aop.LogExecutionTime;
import com.arivanamin.library.book.model.entity.Book;
import com.arivanamin.library.book.model.service.BookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping ("/api/books")
@RequiredArgsConstructor
@Slf4j
public class BooksController {
    
    private final BookService bookService;
    
    @GetMapping
    @ResponseStatus (HttpStatus.OK)
    @LogExecutionTime
    public List<Book> getAllBooks () {
        log.info("Received request to fetch all books");
        List<Book> books = bookService.getAllBooks();
        log.info("Fetched {} Books from Service", books.size());
        return books;
    }
    
    @GetMapping ("/{id}")
    @ResponseStatus (HttpStatus.OK)
    @LogExecutionTime
    public Book getBookDetails (@PathVariable Long id) {
        log.info("Received request to fetch book {} details", id);
        Book book = bookService.getBookDetails(id);
        log.info("fetched book with id of {} from service, book details: {}", id, book);
        return book;
    }
    
    @PostMapping
    @ResponseStatus (HttpStatus.CREATED)
    @LogExecutionTime
    public void createBook (@RequestBody Book book) {
        log.info("Received request to create book with details: {}", book);
        bookService.createBook(book);
        log.info("book created successfully");
    }
    
    @PutMapping ("/{id}")
    @ResponseStatus (HttpStatus.CREATED)
    @LogExecutionTime
    public void updateBook (@PathVariable Long id, @RequestBody Book updatedBook) {
        log.info("Received request to update book {} with details: {}", id, updatedBook);
        bookService.updateBook(id, updatedBook);
        log.info("book updated successfully");
    }
    
    @DeleteMapping ("/{id}")
    @ResponseStatus (HttpStatus.NO_CONTENT)
    @LogExecutionTime
    public void deleteBook (@PathVariable Long id) {
        log.info("Received request to delete book {}", id);
        bookService.deleteBook(id);
        log.info("book deleted successfully");
    }
}
