package com.arivanamin.library.book.infrastructure.controller;

import com.arivanamin.library.book.model.service.BookNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@ControllerAdvice
class BookNotFoundAdvice {
    
    @ResponseBody
    @ExceptionHandler (BookNotFoundException.class)
    @ResponseStatus (HttpStatus.NOT_FOUND)
    String bookNotFoundHandler (BookNotFoundException exception) {
        return exception.getMessage();
    }
}
