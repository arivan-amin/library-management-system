package com.arivanamin.library.book.infrastructure.service;

import com.arivanamin.library.aop.LogExecutionTime;
import com.arivanamin.library.book.infrastructure.repository.BookRepository;
import com.arivanamin.library.book.model.entity.Book;
import com.arivanamin.library.book.model.service.BookNotFoundException;
import com.arivanamin.library.book.model.service.BookService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Locale.ENGLISH;

@Service
@RequiredArgsConstructor
@Slf4j
@CacheConfig (cacheNames = "book")
public class BookServiceImpl implements BookService {
    
    private final BookRepository repository;
    
    @Override
    @LogExecutionTime
    @Cacheable
    public List<Book> getAllBooks () {
        return fetchAllBooksFromRepository();
    }
    
    private List<Book> fetchAllBooksFromRepository () {
        List<Book> books = repository.findAll();
        log.info("fetched from repository list of books: {}", books);
        return books;
    }
    
    @Override
    @LogExecutionTime
    @Cacheable
    public Book getBookDetails (long id) {
        return fetchBookFromRepositoryAndThrowIfNotFound(id);
    }
    
    private Book fetchBookFromRepositoryAndThrowIfNotFound (long id) {
        Book book = findInRepositoryAndThrowExceptionIfNotFound(id);
        log.info("found book with id: {}, book details: {}", id, book);
        return book;
    }
    
    private Book findInRepositoryAndThrowExceptionIfNotFound (long id) {
        return repository.findBookById(id).orElseThrow(BookNotFoundException::new);
    }
    
    @Override
    @LogExecutionTime
    @Transactional
    public void createBook (Book book) {
        logDetailsOfBookToBeCreated(book);
        saveEntityToRepository(book);
    }
    
    private static void logDetailsOfBookToBeCreated (Book book) {
        log.info("received book details to be created: {}", book);
    }
    
    private void saveEntityToRepository (Book book) {
        repository.save(book);
    }
    
    @Override
    @LogExecutionTime
    @Transactional
    public void updateBook (long id, Book updatedBook) {
        logDetailsOfTheReceivedBook(id, updatedBook);
        Book book = fetchBookByIdAndThrowIfNotFound(id);
        updateBookDetailsWithTheValuesInReceivedBook(updatedBook, book);
        logBookDetailsAfterUpdating(book);
        saveEntityToRepository(book);
    }
    
    private static void logDetailsOfTheReceivedBook (long id, Book updatedBook) {
        log.info("received book id: {} and details to be updated: {}", id, updatedBook);
    }
    
    private Book fetchBookByIdAndThrowIfNotFound (long id) {
        Book book = findInRepositoryAndThrowExceptionIfNotFound(id);
        log.info("found book with id: {}, book details before updating: {}", id, book);
        return book;
    }
    
    private void updateBookDetailsWithTheValuesInReceivedBook (Book updatedBook, Book book) {
        book.setIsbn(updatedBook.getIsbn());
        book.setTitle(formatNameToLowercase(updatedBook.getTitle()));
        book.setAuthor(formatNameToLowercase(updatedBook.getAuthor()));
        book.setPublicationYear(updatedBook.getPublicationYear());
    }
    
    private static void logBookDetailsAfterUpdating (Book book) {
        log.info("book details after updating details from the received Book: {}", book);
    }
    
    private static String formatNameToLowercase (String name) {
        return name.toLowerCase(ENGLISH);
    }
    
    @Override
    @LogExecutionTime
    @Transactional
    public void deleteBook (long id) {
        deleteBookInRepositoryAndLogTheDetails(id);
    }
    
    private void deleteBookInRepositoryAndLogTheDetails (long id) {
        repository.deleteById(id);
        log.info("deleted book with the id: {}", id);
    }
}
