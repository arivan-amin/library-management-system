package com.arivanamin.library.book.infrastructure.repository;

import com.arivanamin.library.book.model.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BookRepository extends JpaRepository<Book, Long> {
    
    Optional<Book> findBookById (Long id);
}
