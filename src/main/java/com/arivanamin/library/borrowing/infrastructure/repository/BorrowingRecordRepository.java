package com.arivanamin.library.borrowing.infrastructure.repository;

import com.arivanamin.library.borrowing.model.entity.BorrowingRecord;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface BorrowingRecordRepository extends JpaRepository<BorrowingRecord, Long> {
    
    Optional<BorrowingRecord> findAllByBookIdAndPatronIdAndReturningDateNull (long bookId,
        long patronId);
    
    List<BorrowingRecord> findAllByBookIdAndPatronId (long bookId,
        long patronId);
}
