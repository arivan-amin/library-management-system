package com.arivanamin.library.borrowing.infrastructure.controller;

import com.arivanamin.library.borrowing.model.service.BookAlreadyReturnedException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@ControllerAdvice
class BookAlreadyReturnedAdvice {
    
    @ResponseBody
    @ExceptionHandler (BookAlreadyReturnedException.class)
    @ResponseStatus (HttpStatus.CONFLICT)
    String bookNotFoundHandler (BookAlreadyReturnedException exception) {
        return exception.getMessage();
    }
}
