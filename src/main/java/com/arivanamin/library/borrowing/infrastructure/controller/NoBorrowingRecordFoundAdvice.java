package com.arivanamin.library.borrowing.infrastructure.controller;

import com.arivanamin.library.borrowing.model.service.NoBorrowingRecordFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@ControllerAdvice
class NoBorrowingRecordFoundAdvice {
    
    @ResponseBody
    @ExceptionHandler (NoBorrowingRecordFoundException.class)
    @ResponseStatus (HttpStatus.CONFLICT)
    String bookNotFoundHandler (NoBorrowingRecordFoundException exception) {
        return exception.getMessage();
    }
}
