package com.arivanamin.library.borrowing.infrastructure.controller;

import com.arivanamin.library.borrowing.model.service.BookAlreadyBorrowedException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@ControllerAdvice
class BookAlreadyBorrowedAdvice {
    
    @ResponseBody
    @ExceptionHandler (BookAlreadyBorrowedException.class)
    @ResponseStatus (HttpStatus.CONFLICT)
    String bookNotFoundHandler (BookAlreadyBorrowedException exception) {
        return exception.getMessage();
    }
}
