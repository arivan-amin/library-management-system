package com.arivanamin.library.borrowing.infrastructure.service;

import com.arivanamin.library.aop.LogExecutionTime;
import com.arivanamin.library.book.model.entity.Book;
import com.arivanamin.library.book.model.service.BookService;
import com.arivanamin.library.borrowing.infrastructure.repository.BorrowingRecordRepository;
import com.arivanamin.library.borrowing.model.entity.BorrowingRecord;
import com.arivanamin.library.borrowing.model.service.*;
import com.arivanamin.library.patron.model.entity.Patron;
import com.arivanamin.library.patron.model.service.PatronService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class BorrowingRecordServiceImpl implements BorrowingRecordService {
    
    private final BorrowingRecordRepository repository;
    
    private final BookService bookService;
    
    private final PatronService patronService;
    
    @Override
    @LogExecutionTime
    @Transactional
    public void borrowBook (long bookId, long patronId) {
        logDetailsOfBorrowingRecordToBeCreated(bookId, patronId);
        Book book = getBookDetailsFromRepository(bookId);
        Patron patron = getPatronDetailsFromRepository(patronId);
        if (isBookNotBorrowedByPatron(bookId, patronId)) {
            createBorrowingRecordAndSaveToRepository(book, patron);
        }
        else {
            throw new BookAlreadyBorrowedException();
        }
    }
    
    private static void logDetailsOfBorrowingRecordToBeCreated (long bookId, long patronId) {
        log.info("Creating borrow record for book: {} by patron: {}", bookId, patronId);
    }
    
    private Book getBookDetailsFromRepository (long bookId) {
        return bookService.getBookDetails(bookId);
    }
    
    private Patron getPatronDetailsFromRepository (long patronId) {
        return patronService.getPatronDetails(patronId);
    }
    
    private boolean isBookNotBorrowedByPatron (long bookId, long patronId) {
        return repository.findAllByBookIdAndPatronIdAndReturningDateNull(bookId, patronId)
            .isEmpty();
    }
    
    private void createBorrowingRecordAndSaveToRepository (Book book, Patron patron) {
        BorrowingRecord record = new BorrowingRecord();
        record.setBook(book);
        record.setPatron(patron);
        record.setBorrowingDate(LocalDate.now());
        repository.save(record);
    }
    
    @Override
    @LogExecutionTime
    @Transactional
    public void returnBook (long bookId, long patronId) {
        logDetailsOfBookThatReturned(bookId, patronId);
        if (isAnyRecordFoundForBookAndPatron(bookId, patronId)) {
            updateBorrowingRecordToReturned(bookId, patronId);
        }
        else {
            throw new NoBorrowingRecordFoundException();
        }
    }
    
    private static void logDetailsOfBookThatReturned (long bookId, long patronId) {
        log.info("updating borrow record for book: {} by patron: {} to returned", bookId, patronId);
    }
    
    private boolean isAnyRecordFoundForBookAndPatron (long bookId, long patronId) {
        return !repository.findAllByBookIdAndPatronId(bookId, patronId).isEmpty();
    }
    
    private void updateBorrowingRecordToReturned (long bookId, long patronId) {
        Optional<BorrowingRecord> optional = isBookNotAlreadyReturnedByPatron(bookId, patronId);
        if (optional.isPresent()) {
            setBorrowingRecordStatusToReturned(optional.get());
        }
        else {
            throw new BookAlreadyReturnedException();
        }
    }
    
    private Optional<BorrowingRecord> isBookNotAlreadyReturnedByPatron (long bookId,
        long patronId) {
        return repository.findAllByBookIdAndPatronIdAndReturningDateNull(bookId, patronId);
    }
    
    private void setBorrowingRecordStatusToReturned (BorrowingRecord borrowingRecord) {
        borrowingRecord.setReturningDate(LocalDate.now());
        repository.save(borrowingRecord);
    }
}
