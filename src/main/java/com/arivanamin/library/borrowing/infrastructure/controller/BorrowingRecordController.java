package com.arivanamin.library.borrowing.infrastructure.controller;

import com.arivanamin.library.aop.LogExecutionTime;
import com.arivanamin.library.borrowing.model.service.BorrowingRecordService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping ("/api")
@RequiredArgsConstructor
@Slf4j
public class BorrowingRecordController {
    
    private final BorrowingRecordService borrowingService;
    
    @PostMapping ("/borrow/{bookId}/patron/{patronId}")
    @ResponseStatus (HttpStatus.CREATED)
    @LogExecutionTime
    public void borrowBook (@PathVariable Long bookId, @PathVariable Long patronId) {
        log.info("Received request to borrow book {} by patron: {}", bookId, patronId);
        borrowingService.borrowBook(bookId, patronId);
        log.info("book borrowed successfully");
    }
    
    @PutMapping ("/return/{bookId}/patron/{patronId}")
    @ResponseStatus (HttpStatus.OK)
    @LogExecutionTime
    public void returnBook (@PathVariable Long bookId, @PathVariable Long patronId) {
        log.info("Received request to return book {} by patron: {}", bookId, patronId);
        borrowingService.returnBook(bookId, patronId);
        log.info("book returned successfully");
    }
}
