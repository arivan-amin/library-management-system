package com.arivanamin.library.borrowing.model.service;

public class BookAlreadyBorrowedException extends IllegalArgumentException {
    
    public BookAlreadyBorrowedException () {
        super("Requested Book is already borrowed");
    }
}
