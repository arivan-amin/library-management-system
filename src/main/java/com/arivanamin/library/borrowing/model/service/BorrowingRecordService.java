package com.arivanamin.library.borrowing.model.service;

public interface BorrowingRecordService {
    
    void borrowBook (long bookId, long patronId);
    
    void returnBook (long bookId, long patronId);
}
