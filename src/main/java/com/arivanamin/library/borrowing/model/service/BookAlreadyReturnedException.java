package com.arivanamin.library.borrowing.model.service;

public class BookAlreadyReturnedException extends IllegalArgumentException {
    
    public BookAlreadyReturnedException () {
        super("Requested Book is already returned");
    }
}
