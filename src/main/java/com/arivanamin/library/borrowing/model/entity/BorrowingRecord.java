package com.arivanamin.library.borrowing.model.entity;

import com.arivanamin.library.book.model.entity.Book;
import com.arivanamin.library.patron.model.entity.Patron;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class BorrowingRecord {
    
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;
    
    @ManyToOne
    private Book book;
    
    @ManyToOne
    private Patron patron;
    
    @Column
    private LocalDate borrowingDate;
    
    @Column
    private LocalDate returningDate;
}
