package com.arivanamin.library.borrowing.model.service;

public class NoBorrowingRecordFoundException extends IllegalArgumentException {
    
    public NoBorrowingRecordFoundException () {
        super("No borrowing record was found for the requested book and patron");
    }
}
