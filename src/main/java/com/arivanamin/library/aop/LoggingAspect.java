package com.arivanamin.library.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

@Aspect
@Component
@Slf4j
public class LoggingAspect {
    
    @Around ("(@annotation(org.springframework.web.bind.annotation.GetMapping) || @annotation(org" +
        ".springframework.web.bind.annotation.PostMapping) || " +
        "@annotation(org.springframework.web.bind.annotation.PatchMapping) || @annotation(org" +
        ".springframework.web.bind.annotation.DeleteMapping)) " +
        "&& @annotation(com.arivanamin.library.aop.LogExecutionTime)")
    public Object logExecutionTime (ProceedingJoinPoint joinPoint) throws Throwable {
        Signature methodSignature = joinPoint.getSignature();
        String className = methodSignature.getDeclaringType().getSimpleName();
        String methodName = methodSignature.getName();
        log.info("Starting execution of method: {}.{}()", className, methodName);
        final StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        Object result = joinPoint.proceed();
        stopWatch.stop();
        log.info("Finished execution of method {}.{}() in: {}ms", className, methodName,
            stopWatch.getTotalTimeMillis());
        return result;
    }
}
