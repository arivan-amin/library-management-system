package com.arivanamin.library.data.loaders;

import com.arivanamin.library.book.infrastructure.repository.BookRepository;
import com.arivanamin.library.book.model.entity.Book;
import com.arivanamin.library.patron.infrastructure.repository.PatronRepository;
import com.arivanamin.library.patron.model.entity.Patron;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.util.stream.IntStream;

@Configuration
@RequiredArgsConstructor
class BookAndPatronRepositoryInitializer {
    
    Faker faker = new Faker();
    
    @Bean
    CommandLineRunner initDatabase (BookRepository bookRepository,
        PatronRepository patronRepository) {
        return args -> {
            int numberOfEntities = faker.number().numberBetween(5, 10);
            populateBooksRepositoryWithFakeData(bookRepository, numberOfEntities);
            populatePatronsRepositoryWithFakeData(patronRepository, numberOfEntities);
        };
    }
    
    private void populateBooksRepositoryWithFakeData (BookRepository bookRepository,
        int numberOfEntities) {
        IntStream.rangeClosed(1, numberOfEntities).mapToObj(this::createFakeBook)
            .forEachOrdered(bookRepository::save);
    }
    
    private void populatePatronsRepositoryWithFakeData (PatronRepository patronRepository,
        int numberOfEntities) {
        IntStream.rangeClosed(1, numberOfEntities).mapToObj(this::createFakePatron)
            .forEachOrdered(patronRepository::save);
    }
    
    private Book createFakeBook (long i) {
        Book book = new Book();
        book.setId(i);
        book.setIsbn(faker.numerify("###-##-#####-###"));
        book.setTitle(faker.book().title());
        book.setAuthor(faker.book().author());
        book.setPublicationYear(faker.elderScrolls().firstName());
        book.setPublicationYear(createFakePublicationYear());
        return book;
    }
    
    private Patron createFakePatron (long i) {
        Patron patron = new Patron();
        patron.setId(i);
        patron.setName(faker.elderScrolls().lastName());
        patron.setContactInformation(faker.book().title());
        return patron;
    }
    
    private String createFakePublicationYear () {
        return String.valueOf(
            LocalDate.now().minusYears(faker.number().numberBetween(1, 20)).getYear());
    }
}
