package com.arivanamin.library.patron.infrastructure.service;

import com.arivanamin.library.aop.LogExecutionTime;
import com.arivanamin.library.patron.infrastructure.repository.PatronRepository;
import com.arivanamin.library.patron.model.entity.Patron;
import com.arivanamin.library.patron.model.service.PatronNotFoundException;
import com.arivanamin.library.patron.model.service.PatronService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Locale.ENGLISH;

@Service
@RequiredArgsConstructor
@Slf4j
@CacheConfig (cacheNames = "patron")
public class PatronServiceImpl implements PatronService {
    
    private final PatronRepository repository;
    
    @Override
    @LogExecutionTime
    @Cacheable
    public List<Patron> getAllPatrons () {
        return fetchAllPatronsFromRepository();
    }
    
    private List<Patron> fetchAllPatronsFromRepository () {
        List<Patron> patrons = repository.findAll();
        log.info("fetched from repository list of patrons: {}", patrons);
        return patrons;
    }
    
    @Override
    @LogExecutionTime
    @Cacheable
    public Patron getPatronDetails (long id) {
        return fetchPatronFromRepositoryAndThrowIfNotFound(id);
    }
    
    private Patron fetchPatronFromRepositoryAndThrowIfNotFound (long id) {
        Patron patron = findInRepositoryAndThrowExceptionIfNotFound(id);
        log.info("found patron with id: {}, patron details: {}", id, patron);
        return patron;
    }
    
    private Patron findInRepositoryAndThrowExceptionIfNotFound (long id) {
        return repository.findPatronById(id).orElseThrow(PatronNotFoundException::new);
    }
    
    @Override
    @LogExecutionTime
    @Transactional
    public void createPatron (Patron patron) {
        logDetailsOfPatronToBeCreated(patron);
        saveEntityToRepository(patron);
    }
    
    private static void logDetailsOfPatronToBeCreated (Patron patron) {
        log.info("received patron details to be created: {}", patron);
    }
    
    private void saveEntityToRepository (Patron patron) {
        repository.save(patron);
    }
    
    @Override
    @LogExecutionTime
    @Transactional
    public void updatePatron (long id, Patron updatedPatron) {
        logDetailsOfTheReceivedPatron(id, updatedPatron);
        Patron patron = fetchPatronByIdAndThrowIfNotFound(id);
        updatePatronDetailsWithTheValuesInReceivedPatron(updatedPatron, patron);
        logPatronDetailsAfterUpdating(patron);
        saveEntityToRepository(patron);
    }
    
    private static void logDetailsOfTheReceivedPatron (long id, Patron updatedPatron) {
        log.info("received patron id: {} and details to be updated: {}", id, updatedPatron);
    }
    
    private Patron fetchPatronByIdAndThrowIfNotFound (long id) {
        Patron patron = findInRepositoryAndThrowExceptionIfNotFound(id);
        log.info("found patron with id: {}, patron details before updating: {}", id, patron);
        return patron;
    }
    
    private void updatePatronDetailsWithTheValuesInReceivedPatron (Patron updatedPatron,
        Patron patron) {
        patron.setName(formatNameToLowercase(updatedPatron.getName()));
        patron.setContactInformation(formatNameToLowercase(updatedPatron.getContactInformation()));
    }
    
    private static void logPatronDetailsAfterUpdating (Patron patron) {
        log.info("patron details after updating details from the received Patron: {}", patron);
    }
    
    private static String formatNameToLowercase (String name) {
        return name.toLowerCase(ENGLISH);
    }
    
    @Override
    @LogExecutionTime
    @Transactional
    public void deletePatron (long id) {
        deletePatronInRepositoryAndLogTheDetails(id);
    }
    
    private void deletePatronInRepositoryAndLogTheDetails (long id) {
        repository.deleteById(id);
        log.info("deleted patron with the id: {}", id);
    }
}
