package com.arivanamin.library.patron.infrastructure.repository;

import com.arivanamin.library.patron.model.entity.Patron;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PatronRepository extends JpaRepository<Patron, Long> {
    
    Optional<Patron> findPatronById (Long id);
}
