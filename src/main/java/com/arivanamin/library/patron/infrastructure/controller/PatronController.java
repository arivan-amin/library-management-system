package com.arivanamin.library.patron.infrastructure.controller;

import com.arivanamin.library.aop.LogExecutionTime;
import com.arivanamin.library.patron.model.entity.Patron;
import com.arivanamin.library.patron.model.service.PatronService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping ("/api/patrons")
@RequiredArgsConstructor
@Slf4j
public class PatronController {
    
    private final PatronService patronService;
    
    @GetMapping
    @ResponseStatus (HttpStatus.OK)
    @LogExecutionTime
    public List<Patron> getAllPatrons () {
        log.info("Received request to fetch all patrons");
        List<Patron> patrons = patronService.getAllPatrons();
        log.info("Fetched {} Patrons from Service", patrons.size());
        return patrons;
    }
    
    @GetMapping ("/{id}")
    @ResponseStatus (HttpStatus.OK)
    @LogExecutionTime
    public Patron getPatronDetails (@PathVariable Long id) {
        log.info("Received request to fetch patron {} details", id);
        Patron patron = patronService.getPatronDetails(id);
        log.info("fetched patron with id of {} from service, patron details: {}", id, patron);
        return patron;
    }
    
    @PostMapping
    @ResponseStatus (HttpStatus.CREATED)
    @LogExecutionTime
    public void createPatron (@RequestBody Patron patron) {
        log.info("Received request to create patron with details: {}", patron);
        patronService.createPatron(patron);
        log.info("patron created successfully");
    }
    
    @PutMapping ("/{id}")
    @ResponseStatus (HttpStatus.CREATED)
    @LogExecutionTime
    public void updatePatron (@PathVariable Long id, @RequestBody Patron updatedPatron) {
        log.info("Received request to update patron {} with details: {}", id, updatedPatron);
        patronService.updatePatron(id, updatedPatron);
        log.info("patron updated successfully");
    }
    
    @DeleteMapping ("/{id}")
    @ResponseStatus (HttpStatus.NO_CONTENT)
    @LogExecutionTime
    public void deletePatron (@PathVariable Long id) {
        log.info("Received request to delete patron {}", id);
        patronService.deletePatron(id);
        log.info("patron deleted successfully");
    }
}
