package com.arivanamin.library.patron.infrastructure.controller;

import com.arivanamin.library.patron.model.service.PatronNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@ControllerAdvice
class PatronNotFoundAdvice {
    
    @ResponseBody
    @ExceptionHandler (PatronNotFoundException.class)
    @ResponseStatus (HttpStatus.NOT_FOUND)
    String bookNotFoundHandler (PatronNotFoundException exception) {
        return exception.getMessage();
    }
}
