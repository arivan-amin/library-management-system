package com.arivanamin.library.patron.model.service;

import com.arivanamin.library.patron.model.entity.Patron;

import java.util.List;

public interface PatronService {
    
    List<Patron> getAllPatrons ();
    
    Patron getPatronDetails (long id);
    
    void createPatron (Patron book);
    
    void updatePatron (long id, Patron updatedPatron);
    
    void deletePatron (long id);
}
