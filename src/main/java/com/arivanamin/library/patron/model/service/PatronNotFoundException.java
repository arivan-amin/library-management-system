package com.arivanamin.library.patron.model.service;

public class PatronNotFoundException extends IllegalArgumentException {
    
    public PatronNotFoundException () {
        super("Requested Patron couldn't be found");
    }
}
